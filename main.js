// Class ListPerson
const listPerson = new ListPerson();

// Fetch ListPerson
function fetchListPerson() {
  const list = listPerson.getAll();
  renderListPerson(list);
  document.getElementById("filterByType").value = "";
  document.getElementById("sortByProps").value = "";
  $("#myModal").modal("hide");
}

// Reset input form modal
closeInputModal();
// Mở input form theo loại người dùng được chọn trong modal
function handleSelectUser(event) {
  let type = event.value;
  if (type == "") {
    closeInputModal();
  } else {
    openInputModal(type);
  }
}

// Kiểm tra validate chung ngoại trừ id
function checkValidCommonExceptId(per) {
  let isValid = checkEmpty("tbHoTen", per.hoTen) && checkHoTen(per.hoTen);
  isValid &= checkEmpty("tbEmail", per.email) && checkEmail(per.email);
  isValid &= checkEmpty("tbDiaChi", per.diaChi);
  return isValid;
}

// Kiểm tra validate Sinh viên
function checkValidStudent(per) {
  let isValid = checkValidCommonExceptId(per);
  isValid &=
    checkEmpty("tbDiemToan", per.toan) &&
    checkValueNumber(0, 10, "tbDiemToan", per.toan);
  isValid &=
    checkEmpty("tbDiemLy", per.ly) &&
    checkValueNumber(0, 10, "tbDiemLy", per.ly);
  isValid &=
    checkEmpty("tbDiemHoa", per.hoa) &&
    checkValueNumber(0, 10, "tbDiemHoa", per.hoa);
  return isValid;
}

// Kiểm tra validate Nhân viên
function checkValidEmployee(per) {
  let isValid = checkValidCommonExceptId(per);
  isValid &= checkEmpty("tbSoNgayLam", per.soNgay);
  isValid &= checkEmpty("tbLuongNgay", per.luongNgay);
  return isValid;
}

// Kiểm tra validate Khách hàng
function checkValidCustomer(per) {
  let isValid = checkValidCommonExceptId(per);
  isValid &= checkEmpty("tbTenCTy", per.tenCTy);
  isValid &= checkEmpty("tbTGiaHDon", per.giaHDon);
  isValid &= checkEmpty("tbDanhGia", per.danhGia);
  return isValid;
}

// Thêm người dùng
function themNguoiDung() {
  let per = layThongTinTuForm();
  let isValidMa = checkEmpty("tbMa", per.ma) && checkExistId(per.ma);
  let type = document.getElementById("typeUser").value;
  switch (type) {
    case "Student":
      if (isValidMa & checkValidStudent(per)) {
        listPerson.add(
          new Student(
            per.ma,
            per.hoTen,
            per.email,
            per.diaChi,
            per.toan,
            per.ly,
            per.hoa
          )
        );
        return fetchListPerson();
      }
      break;
    case "Employee":
      if (isValidMa & checkValidEmployee(per)) {
        listPerson.add(
          new Employee(
            per.ma,
            per.hoTen,
            per.email,
            per.diaChi,
            per.soNgay,
            per.luongNgay
          )
        );
        return fetchListPerson();
      }
      break;
    case "Customer":
      if (isValidMa & checkValidCustomer(per)) {
        listPerson.add(
          new Customer(
            per.ma,
            per.hoTen,
            per.email,
            per.diaChi,
            per.tenCTy,
            per.giaHDon,
            per.danhGia
          )
        );
        return fetchListPerson();
      }
      break;
    default:
      return;
  }
}

// Xóa người dùng
function xoaNguoiDung(ma) {
  listPerson.delete(ma);
  fetchListPerson();
}

// Sửa người dùng
function suaNguoiDung(ma) {
  let per = listPerson.getById(ma);
  showThongTinLenForm(per);
  openInputModal(per.constructor.name);
  document.getElementById("typeUser").disabled = true;
  document.getElementById("ma").disabled = true;
  document.getElementById("btnCapNhat").disabled = false;
  document.getElementById("btnThem").disabled = true;
}

// Cập nhật người dùng
function capNhatNguoiDung() {
  let type = document.getElementById("typeUser").value;
  let per = layThongTinTuForm();
  switch (type) {
    case "Student":
      if (checkValidStudent(per)) {
        listPerson.update(
          new Student(
            per.ma,
            per.hoTen,
            per.email,
            per.diaChi,
            per.toan,
            per.ly,
            per.hoa
          )
        );
        return fetchListPerson();
      }
      break;
    case "Employee":
      if (checkValidEmployee(per)) {
        listPerson.update(
          new Employee(
            per.ma,
            per.hoTen,
            per.email,
            per.diaChi,
            per.soNgay,
            per.luongNgay
          )
        );
        return fetchListPerson();
      }
      break;
    case "Customer":
      if (checkValidCustomer(per)) {
        listPerson.update(
          new Customer(
            per.ma,
            per.hoTen,
            per.email,
            per.diaChi,
            per.tenCTy,
            per.giaHDon,
            per.danhGia
          )
        );
        return fetchListPerson();
      }
      break;
    default:
      return;
  }
}

// Lọc danh sách theo loại người dùng và theo sắp xếp (mã hoặc họ tên)
function handleChangeSelected() {
  let type = document.getElementById("filterByType").value;
  let otherProps = document.getElementById("sortByProps").value;
  let currentList = listPerson.filterAndSort(type, otherProps);
  renderListPerson(currentList);
}

// Tạo hàm trả về dom theo Id
function DOM_ID(id) {
  return document.getElementById(id);
}

// Xem tính năng
function xemTinhNang(ma, type) {
  $("#featureModal").modal("show");
  if (type === "Student") {
    DOM_ID("featureModalLabel").innerText = "Tính điểm trung bình";
    let stu = listPerson.getById(ma);
    DOM_ID("pInforFeature1").innerHTML = `Toán: <span>${stu.toan} điểm</span>`;
    DOM_ID("pInforFeature2").innerHTML = `Lý: <span>${stu.ly} điểm</span>`;
    DOM_ID("pInforFeature3").innerHTML = `Hóa: <span>${stu.hoa} điểm</span>`;
    DOM_ID(
      "pResultFeature"
    ).innerHTML = `Điểm trung bình: ${stu.tinhDTB()} điểm`;
  } else if (type === "Employee") {
    DOM_ID("featureModalLabel").innerText = "Tính lương";
    let emp = listPerson.getById(ma);
    DOM_ID(
      "pInforFeature1"
    ).innerHTML = `Số ngày làm: <span>${emp.soNgay}</span>`;
    DOM_ID(
      "pInforFeature2"
    ).innerHTML = `Lương theo ngày: <span>${emp.luongNgay.toLocaleString()}<u>đ</u></span>`;
    DOM_ID("pInforFeature3").innerHTML = "";
    DOM_ID("pResultFeature").innerHTML = `Tổng lương: ${emp
      .tinhLuong()
      .toLocaleString()}<u>đ</u>`;
  } else if (type === "Customer") {
    DOM_ID("featureModalLabel").innerText = "Chi tiết khách hàng";
    let cus = listPerson.getById(ma);
    DOM_ID(
      "pInforFeature1"
    ).innerHTML = `Tên công ty: <span>${cus.tenCTy}</span>`;
    DOM_ID(
      "pInforFeature2"
    ).innerHTML = `Trị giá hóa đơn: <span>${cus.giaHDon.toLocaleString()}<u>đ</u></span>`;
    DOM_ID(
      "pInforFeature3"
    ).innerHTML = `Đánh giá: <span>${cus.danhGia}</span>`;
    DOM_ID("pResultFeature").innerHTML = "";
  }
}
