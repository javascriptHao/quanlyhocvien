// Render danh sách người dùng
function renderListPerson(list) {
  let contentHTML = "";
  list.forEach((item) => {
    let type = item.constructor.name;
    let loaiPerson = "";
    if (type === "Student") loaiPerson = "Sinh viên";
    else
      type === "Employee"
        ? (loaiPerson = "Nhân viên")
        : (loaiPerson = "Khách hàng");
    let content = `<tr>
    <td>${item.ma}</td>
    <td>${item.hoTen}</td>
    <td>${item.email}</td>
    <td>${item.diaChi}</td>
    <td>${loaiPerson}</td>
    <td><button class="btn btn-success" onclick="xemTinhNang('${item.ma}','${type}')">Xem</button></td>
    <td><button onclick="suaNguoiDung('${item.ma}')" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Sửa</button>
    <button onclick="xoaNguoiDung('${item.ma}')" class="btn btn-danger">Xóa</button>
    </td>
    </tr>`;
    contentHTML += content;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// Lấy thông tin từ form
function layThongTinTuForm() {
  let ma = document.getElementById("ma").value;
  let hoTen = document.getElementById("hoTen").value;
  let email = document.getElementById("email").value;
  let diaChi = document.getElementById("diaChi").value;
  let toan = document.getElementById("toan").value;
  let ly = document.getElementById("ly").value;
  let hoa = document.getElementById("hoa").value;
  let soNgay = document.getElementById("soNgay").value;
  let luongNgay = document.getElementById("luongNgay").value;
  let tenCTy = document.getElementById("tenCTy").value;
  let giaHDon = document.getElementById("giaHDon").value;
  let danhGia = document.getElementById("danhGia").value;
  return {
    ma,
    hoTen,
    email,
    diaChi,
    toan,
    ly,
    hoa,
    soNgay,
    luongNgay,
    tenCTy,
    giaHDon,
    danhGia,
  };
}

// Show thông tin lên form
function showThongTinLenForm(per) {
  document.getElementById("typeUser").value = per.constructor.name;
  document.getElementById("ma").value = per.ma;
  document.getElementById("hoTen").value = per.hoTen;
  document.getElementById("email").value = per.email;
  document.getElementById("diaChi").value = per.diaChi;
  document.getElementById("toan").value = per.toan;
  document.getElementById("ly").value = per.ly;
  document.getElementById("hoa").value = per.hoa;
  document.getElementById("soNgay").value = per.soNgay;
  document.getElementById("luongNgay").value = per.luongNgay;
  document.getElementById("tenCTy").value = per.tenCTy;
  document.getElementById("giaHDon").value = per.giaHDon;
  document.getElementById("danhGia").value = per.danhGia;
}

// Reset lại modal
function resetModal() {
  document.getElementById("formQLNV").reset();
  document.getElementById("typeUser").disabled = false;
  document.getElementById("btnCapNhat").disabled = true;
  document.getElementById("btnThem").disabled = false;
  closeInputModal();
}

// Đóng input form modal
function closeInputModal() {
  let inputChung = document.querySelectorAll(".inputChung");
  for (let val of inputChung) {
    val.disabled = true;
  }
  let inputRieng = document.querySelectorAll(".inputRieng");
  for (let val of inputRieng) {
    val.style.display = "none";
  }
}

// Mở input form modal
function openInputModal(type) {
  let inputChung = document.querySelectorAll(".inputChung");
  for (let val of inputChung) {
    val.disabled = false;
  }
  let inputRieng = document.querySelectorAll(".inputRieng");
  for (let val of inputRieng) {
    val.style.display = "none";
  }
  let inputType = `.input${type}`;
  let arr = document.querySelectorAll(inputType);
  for (let val of arr) {
    val.style.display = "block";
  }
}
