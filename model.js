// Class Person
class Person {
  constructor(ma, hoTen, email, diaChi) {
    this.ma = ma;
    this.hoTen = hoTen;
    this.email = email;
    this.diaChi = diaChi;
  }
}

// Class Student (kế thừa class Person)
class Student extends Person {
  constructor(ma, hoTen, email, diaChi, toan, ly, hoa) {
    super(ma, hoTen, email, diaChi);
    this.toan = toan * 1;
    this.ly = ly * 1;
    this.hoa = hoa * 1;
  }
  tinhDTB() {
    return Math.round((this.toan + this.ly + this.hoa) / 3);
  }
}

// Class Employee (kế thừa class Person)
class Employee extends Person {
  constructor(ma, hoTen, email, diaChi, soNgay, luongNgay) {
    super(ma, hoTen, email, diaChi);
    this.soNgay = soNgay * 1;
    this.luongNgay = luongNgay * 1;
  }
  tinhLuong() {
    return this.soNgay * this.luongNgay;
  }
}

// Class Customer (kế thừa class Person)
class Customer extends Person {
  constructor(ma, hoTen, email, diaChi, tenCTy, giaHDon, danhGia) {
    super(ma, hoTen, email, diaChi);
    this.tenCTy = tenCTy;
    this.giaHDon = giaHDon * 1;
    this.danhGia = danhGia;
  }
}

// Class ListPerson (quản lý mảng chứa các class)
class ListPerson {
  constructor() {
    this.persons = [];
  }
  add(person) {
    this.persons.push(person);
  }
  delete(ma) {
    this.persons = this.persons.filter((item) => item.ma != ma);
  }
  getById(ma) {
    let person = this.persons.find((item) => item.ma == ma);
    return person;
  }
  getAll() {
    return this.persons;
  }
  update(person) {
    let index = this.persons.findIndex((item) => item.ma == person.ma);
    this.persons[index] = person;
  }
  filterAndSort(type, otherProps) {
    let filterArr = [];
    if (type == "") {
      filterArr = this.persons;
    } else {
      filterArr = this.persons.filter((item) => item.constructor.name == type);
    }
    if (otherProps == "") {
      return filterArr;
    } else if (otherProps == "byMa") {
      return filterArr.sort((a, b) => a.ma - b.ma);
    } else if (otherProps == "byHoTen") {
      return filterArr.sort((a, b) => a.hoTen.localeCompare(b.hoTen));
    }
  }
}
